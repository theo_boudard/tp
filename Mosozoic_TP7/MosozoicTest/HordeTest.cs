﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using MosozoicConsole;


namespace MosozoicTest
{
    [TestClass]
    public class HordeTest
    {
        [TestMethod]
        public void TestAddDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.Empty(horde.GetDinosaurs());
            horde.AddDinosaur(louis);
            Assert.Single(horde.GetDinosaurs());
            Assert.AreEqual(louis, horde.GetDinosaurs()[0]);
            horde.AddDinosaur(nessie);
            Assert.AreEqual(2, horde.GetDinosaurs().Count);
            Assert.AreEqual(nessie, horde.GetDinosaurs()[1]);
        }

        [TestMethod]
        public void TestRemoveDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);

            Assert.AreEqual(2, horde.GetDinosaurs().Count);
            horde.RemoveDinosaur(louis);
            Assert.Single(horde.GetDinosaurs());
            horde.RemoveDinosaur(nessie);
            Assert.Empty(horde.GetDinosaurs());
        }


        [TestMethod]
        public void TestIntroduceAll()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);

            string expected_introduction = "Je suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n";

            Assert.AreEqual(expected_introduction, horde.IntroduceAll());
        }
    }
}

