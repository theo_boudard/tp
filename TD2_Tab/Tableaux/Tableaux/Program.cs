﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableaux
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] tab_valeur = new int[10];
            int position_valeur_recherche, valeur_recherche, indice;
            string tab_valeur_string, valeur_recherche_string;

            position_valeur_recherche = -1;

            Console.WriteLine("Saisir 10  valeurs :");
            for (indice = 0; indice <= 9; indice++)
            {
                tab_valeur_string = Console.ReadLine();
                int.TryParse(tab_valeur_string, out tab_valeur[indice]);
            }
            Console.WriteLine("Saissisez la valeur recherché :");
            valeur_recherche_string = Console.ReadLine();
            int.TryParse(valeur_recherche_string, out valeur_recherche);
            indice = 0;

            while (indice < tab_valeur.Length && position_valeur_recherche == -1)
            {
                if (tab_valeur[indice] == valeur_recherche)
                {
                    position_valeur_recherche = indice + 1;


                }
                indice++;
            }
            position_valeur_recherche = Retenue(position_valeur_recherche, valeur_recherche);
            
           
        }

        public static int Retenue(int position_valeur_recherche, int valeur_recherche)
        {
            
            if (position_valeur_recherche == -1)
            {
                Console.WriteLine("La valeur n'est pas dans le tableau");
            }
            else
            {
                Console.WriteLine("La valeur {0} est la {1} eme valeur recherché", valeur_recherche, position_valeur_recherche);
            }


            return position_valeur_recherche;
        }

        


    }
}
