﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TD4;

namespace Test_1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Aire_carre()
        {
            Assert.AreEqual(4, Program.Aire_carré(2));
            Assert.AreEqual(16, Program.Aire_carré(4));
            Assert.AreEqual(25, Program.Aire_carré(5));

        }
        [TestMethod]
        public void Aire_rectangle()
        {
            Assert.AreEqual(18, Program.Aire_rectangle(6, 3));
            Assert.AreEqual(70, Program.Aire_rectangle(7, 10));
            Assert.AreEqual(55, Program.Aire_rectangle(11, 5));
        }
        [TestMethod]
        public void Aire_triangle()
        {
            Assert.AreEqual(3, Program.Aire_triangle(3, 2));
            Assert.AreEqual(9, Program.Aire_triangle(6, 3));
            Assert.AreEqual(10, Program.Aire_triangle(10, 2));
        }
    }
}
