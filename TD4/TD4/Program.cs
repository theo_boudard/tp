﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD4
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("HelloWorld");
        }
        public static int Aire_carré(int cote)
        {
            return cote*cote;
        }
        public static int Aire_rectangle(int longueur, int largeur)
        {
            return longueur * largeur;
        }
        public static float Aire_triangle(float bas, float hauteur)
        {
            return (bas * hauteur) / 2;
        }
    }
}

