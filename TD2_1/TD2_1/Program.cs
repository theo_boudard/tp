﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD2_1
{
    public class Program
    {
        public static char Majuscule(char prem_lettre)
        {
            if(prem_lettre >= 'A' && prem_lettre <= 'Z')
            {
                Console.WriteLine("Cette phrase commence par une majuscule");
            }
            else
            {
                Console.WriteLine("Cette phrase ne commence pas par une majuscule");
            }

            return prem_lettre;
        }

        public static char Point(char dern_lettre)
        {
            
            if (dern_lettre == '.')
            {
                Console.WriteLine("Cette phrase finit par un point");

            }
            else
            {
                Console.WriteLine("Cette phrase ne finit pas par un point");
            }
            return dern_lettre;
        }



        static void Main(string[] args)
        {
            char ma_phrase, premiere_lettre, derniere_lettre;
            string ma_phrase_string;
            Console.WriteLine("Saisir votre phrase");
            ma_phrase_string = Console.ReadLine();
            char.TryParse(ma_phrase_string, out ma_phrase);
            premiere_lettre = ma_phrase_string[0];
            premiere_lettre = Majuscule(premiere_lettre);
            derniere_lettre = ma_phrase_string[ma_phrase_string.Length - 1];
            derniere_lettre = Point(derniere_lettre);

            
        }
    }

}
