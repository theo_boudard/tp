﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TD2_1;

namespace Test_1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Majuscule()
        {
            Assert.IsTrue('A' <= Program.Majuscule('S'));
            Assert.IsTrue('Z' >= Program.Majuscule('Q'));
        }


        [TestMethod]
        public void Point()
        {
            Assert.IsTrue('.' == Program.Point('.'));

        }
    }
}
